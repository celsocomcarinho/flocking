using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockManager : MonoBehaviour 
{
    public GameObject fishPrefab; //prefab do peixe com Flock.cs
    public int numFish = 20; //quantidade de peixes a serem gerados neste cardume
    public GameObject[] allFish; //recebe todos os peixes do cardume a serem gerenciados
    public Vector3 swinLimits = new Vector3(5, 5, 5); //limites para destino
    public Vector3 goalPos; //destino

    [Header("Configurações do Cardume")]
    [Range(0.0f, 5.0f)]
    public float minSpeed;
    [Range(0.0f, 5.0f)]
    public float maxSpeed; 
    [Range(1.0f, 10.0f)]
    public float neighbourDistance; //faz nadar mais proximos um dos outros
    [Range(0.0f, 5.0f)]
    public float rotationSpeed; //faz eles se concentrarem ou se dispersarem mais

    private void Start() {
        //define a quantidade de peixes a serem instanciados
        allFish = new GameObject[numFish];

        //instancia e prepara cada peixe na scene
        for(int i = 0; i < numFish; i++) {
            Vector3 pos = GenerateRandomSwimPos(); //posicoes aleatorias para spawn de cada peixe
            allFish[i] = (GameObject) Instantiate(fishPrefab, pos, Quaternion.identity); //spawn
            allFish[i].GetComponent<Flock>().myManager = this; //pega o script Flock de cada peixe instanciado
        }
        //zera o destino inicial
        goalPos = this.transform.position;
    }

    private void Update() {
        goalPos = GenerateRandomSwimPos(); //novos destinos aleatorios
    }

    //metodo pra nao ter que digitar tudo isso denovo
    //sorteia novos destinos dentro dos limites
    Vector3 GenerateRandomSwimPos(){
        return 
            this.transform.position +
            new Vector3(Random.Range(-swinLimits.x, swinLimits.x), 
                        Random.Range(-swinLimits.y, swinLimits.y), 
                        Random.Range(-swinLimits.z, swinLimits.z));
    }
}