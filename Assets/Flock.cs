using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flock : MonoBehaviour
{
    public FlockManager myManager; //gerenciador na scene
    public float speed; //velocidade do peixe (atualizado pelo manager)
    bool turning = false; //verifica se o peixe deve virar

    private void Start() {
        //velocidade aleatória inicial, dentro dos limites estabelecidos
        speed = Random.Range(myManager.minSpeed, myManager.maxSpeed);
    }

    private void Update() {

        //limita a distancia que cada peixe pode se afastar
        Bounds b = new Bounds(myManager.transform.position, myManager.swinLimits * 2);

        //verificacao do reflect
        RaycastHit hit = new RaycastHit();
        Vector3 direction = myManager.transform.position - transform.position;

        //turning = !b.Contains(transform.position) ? true : false;
        //está dentro dos limites do cardume?
        if(!b.Contains(transform.position))
        {
            turning = true;
            //muda a direcao do peixe
            direction = myManager.transform.position - transform.position;
        }
        //verifica se tem obstaculo na frente
        else if (Physics.Raycast(transform.position, this.transform.forward * 50, out hit))
        {
            turning = true;
            //muda a direcao do peixe para nao bater no obstaculo
            direction = Vector3.Reflect(this.transform.forward, hit.normal);
        }
        else
            turning = false;

        //se ta virando, vira
        if(turning){
            transform.rotation = Quaternion.Slerp(  transform.rotation, 
                                                    Quaternion.LookRotation(direction),
                                                    myManager.rotationSpeed * Time.deltaTime);
        }
        else { //se está dentro dos limites do cardume, sorteia
            if (Random.Range(0,100) > 10) //sorteia a velocidade do peixe
                speed = Random.Range(myManager.minSpeed, myManager.maxSpeed);
            if (Random.Range(0,100) < 20) 
                ApplyRules(); //aplica o comportamento de cardume
        }
        //diminui a velicadade ate parar se nao tiver um destino estabelecido
        transform.Translate(0, 0, Time.deltaTime * speed);    
    }

    void ApplyRules() {
        GameObject[] gos; 
        gos = myManager.allFish; //recebe todos os peixes do gerenciador

        Vector3 vcentre = Vector3.zero; //centro do cardume
        Vector3 vavoid = Vector3.zero;  //posicao a ser desviada para nao bater em outros peixes
        float gSpeed = 0.01f; //velocidade do grupo
        float nDistance; //para calculo de distancias em geral
        int groupSize = 0; //tamanho do grupo

        //calcula distacia com os demais para buscar agrupamento
        foreach (GameObject go in gos) {

            if (go != this.gameObject){ //se nao for esse
                //checa a distancia em relacao a cada outro peixe
                nDistance = Vector3.Distance(go.transform.position, this.transform.position);

                //se estiver próximo, adiciona ao grupo e muda o centro do grupo
                if (nDistance <= myManager.neighbourDistance){
                    vcentre += go.transform.position;
                    groupSize++;

                    if (nDistance < 1.0) //se estiver muito próximo, muda a direcao para nao bater
                        vavoid += (this.transform.position - go.transform.position);

                    //gera um novo grupo
                    Flock anotherFlock = go.GetComponent<Flock>(); 
                    gSpeed += anotherFlock.speed; //atualiza a velocidade em relacao ao grupo
                }
            }
        }
        
        //se o grupo já estiver formado
        if(groupSize > 0){
            //atualiza o centro do grupo em relacao a quantidade de peixes e suas posicoes
            vcentre = vcentre / groupSize + (myManager.goalPos - this.transform.position);
            speed = gSpeed / groupSize; //atualiza a velocidade do peixe em relacao a velocidade do grupo

            //atualiza a direcao do peixe de acordo com o centro do grupo e evitando colisoes
            Vector3 direction = (vcentre + vavoid) - transform.position;
            //atualiza a rotacao do peixe apenas de estiver se movimentando
            if (direction != Vector3.zero){
                transform.rotation = Quaternion.Slerp ( transform.rotation, 
                                                        Quaternion.LookRotation(direction),
                                                        myManager.rotationSpeed * Time.deltaTime );
            }
        }
    }
}